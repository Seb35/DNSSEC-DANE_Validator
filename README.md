DNSSEC/DANE Validator
=====================

This was a [Firefox extension](https://web.archive.org/web/20230609185834/https://addons.mozilla.org/en-US/firefox/addon/dnssec-dane-validator/) (now deleted), which added an icon in the address bar to display the status of [DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions) for the visited website, as well as the status of [DANE](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities).

Initial development by defkev (not me, and I don’t know them), from 0.0.2 (8 April 2021) to 0.0.8 (23 December 2021). It was deleted from Mozilla Add-Ons in (around) March 2024; this reverse engineering was done in August 2023.

This repository is archived as the original extension, I will upgrade it under the name [DNSSEC-DANE\_Padlock](https://codeberg.org/Seb35/DNSSEC-DANE_Padlock).

Reverse engineering
-------------------

Although the sources (HTML, JavaScript, CSS) are available in the distributed form (Firefox extensions are basically a ZIP), the main JavaScript file is minified and bundled with a few librairies.

The original source files were not published, and this was for me an exercise about reverse-minification.

I re-created the original source with the following method (although, obviously, I had been creative in variable names):
1. I unminified the JS to see the structure (\*),
2. The bundling comes from BrowserifyJS and aims at bundling npm librairies into a single browser script,
3. The minification is UglifyJS, a well-known (and historical) minifier often used with Browserify,
3. I extracted the main module (the original script) into a standalone JS,
4. I renamed the minified variable names into meaningful variable names, deduced with the dependencies names (the npm modules) and the named fields of the objects,
5. I declared the two root librairies, studying the evolution of these librairies to deduce the versions,
6. For the dependencies of these libraries, given the time has spent and the version constraints are soft, a nowadays (2 years later) npm-install gives a different result; this was a long (few hours) loop:
   1. pick up a module, which is different between the nowadays version (according to the version constraints) and the compiled result,
   2. quickly study the evolution of the library and compare with the date of the original publication,
   3. set a version in package.json
   4. compile with BrowserifyJS, minify with UglifyJS, unminify with the same unminifier than the compiled result (to get a line-for-line diff),
   5. loop until finding the exact version of each library, including dependencies of dependencies of …[finite recursion]

(\*) I used https://unminify.com which does a good job


Deploy
------

To "compile"/minify this extension:
```
cd src
npm install
npm run build
cd ..
```

The result is the file `dnssec-dane-validator@defkev.xpi`.


Generated files
---------------

Although the .xpi should be reproducible with the instruction above, here are the SHA256 of each file to help debugging possible differences:
```
35fdcbe22b6f5cc500ad69deb63814528f6fb13c1ab771c3a78fca6a5b8d70fa  background.js
caafa8ded777a2d0a5793d50d78474486bb88994ba9046f3cb281a04efcea04d  blocked.css
addb4bc165e1666649a162daca47cefc8936dea6c29ef7447b924bbec7911868  blocked.html
2374750b95c0867568ef3b7a2889e45611929ac4a6b772e90fab431b4cacbff0  blocked.js
3b7aca23679c96d113cce925b2a799554e302d3f4895861e51f8da475116fe37  check-circle-solid.svg
1e9df1600439502c402c72e60dcf32c133a7080f61750d88827b14b10bab521c  exclamation-circle-solid.svg
12dacb2812e1a6d35e75c47a4f0647c73816dcbfa5484fb520054847342eb551  LICENSE.md
1f0bed4b4e138a1352ec36f37e3e813853073a07728502c19a2df712a0a39da5  manifest.json
88d1aaa46325f49bd57ccc9b453246a6379536a163fd3c66e2a48bcfdf0b8e95  options.css
a0031bb5c2bff655621cb8270f1b21072d6e6d4aec45c33358723cbac9644ecb  options.html
4c7c479f486731837f66ebdaa8b19ed310b0ad4177d03d20895a73167ad1fd63  options.js
4c68bfba9b3212f46bcbabacb6d685885a2527acca6a6cebfa2ccbd4706fb220  popup.css
637361259ab899e6d7b6c932ac30308432715452ebba40045160a316b127bb5b  popup.html
6b67d08a16d3905d89b0bace73b087e2b90cc169423237401e3ca562ab78be86  popup.js
5a28c8f1e21ea07fde3ef61263b1a46b769e3e68c7f560a8b7b3b7bf913f357d  question-circle-solid.svg
6a3f40a35f6045d58e21db682d60b1d80e53f74c457c5f42d657865d6baf022d  README.md
1c26aed001ca5453153b59470fee99e88774a01e116c9031900ebe701d623212  times-circle-solid.svg
c8eb47a1c859d401a7cb80d611ebf08dc58b79ec052a26c56c5152aa33e51a46  META-INF/cose.manifest
1d52c58535e9a40fc810b110797056107664654884e6e0545c2bc9aac6be3cb1  META-INF/cose.sig
0f606127d1b18367ea23220c11bdc7302dc987b48730f1db12189052fe9c072f  META-INF/manifest.mf
2a39a2ce216ef4b6bef577ba5445b0e8ca25eb3b1f6691f2507ab9a262844b41  META-INF/mozilla.rsa
20fd57ae4cb32d4b1526d02b63e4f5a410ad30b27b9a23c1aa229eb08d54e318  META-INF/mozilla.sf
```

The SHA256 of original and regenerated .xpi are:
```
e8e937f4ae97da2e2844cbfa34df8c133949dc4d1bbfee40dbf451e5d4b54a5b  dnssec-dane-validator@defkev.orig.xpi
98b4a6d3c44cf748b88cc505f1a6786f2d22ef3fcdd93f18407bd2aedb02aa60  dnssec-dane-validator@defkev.xpi
```

The regenerated .xpi here have the exact same content as the original .xpi, except the whole directory META-INF (absent from the regenerated .xpi).


Licence
-------

GNU General Public License v3.0

(according to the description on [https://addons.mozilla.org/en-US/firefox/addon/dnssec-dane-validator/](https://web.archive.org/web/20230609185834/https://addons.mozilla.org/en-US/firefox/addon/dnssec-dane-validator/))
