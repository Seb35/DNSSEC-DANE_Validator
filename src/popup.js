let data = JSON.parse(atob(new URLSearchParams(location.search).getAll('data')))
let header = document.querySelector('header')
let headerIcon = document.querySelector('.icon-section-header>object')
let headerText = document.querySelector('.text-section-header')
let host = document.querySelector('#host')
let status = document.querySelector('#status')
let valid = {
    true: 'check-circle-solid',
    false: 'times-circle-solid',
    null: 'question-circle-solid',
    dane: 'exclamation-circle-solid'
}

headerIcon.setAttribute('data', valid[data.error ? null : data.cache.dane.valid === false ? 'dane' : data.cache.dnssec.valid] + '.svg')
host.innerText = data.hostname
status.innerText = `${data.error ? data.error : (data.cache.dnssec.valid == null ? 'No ' : '') + 'DNSSEC ' + (data.cache.dnssec.valid == null ? '' : data.cache.dnssec.valid ? 'secured' : 'invalid') + (data.cache.dane.valid == null || !data.secure ? '' : ' - DANE ' + (data.cache.dane.valid ? 'valid' : 'failed'))}`

if (data.cache) {
    // I'd rather change the html background color here but this renders a black border at times
    header.style.backgroundColor = data.cache.dnssec.valid ? (data.cache.dane.valid === false && data.secure ? 'DarkOrange' : 'LimeGreen') : (data.cache.dnssec.valid == null ? 'Gold' : 'OrangeRed')
    headerIcon.style.filter = `invert(${data.cache.dnssec.valid == null ? 0 : 100}%)`
    headerText.style.color = data.cache.dnssec.valid == null ? 'Black' : 'White'
    if (data.cache.dane.valid && data.secure) {
        let list = document.createElement('div')
        list.classList.add('panel-section')
        list.classList.add('panel-section-list')
        header.insertAdjacentElement('afterend', list)
        let item = document.createElement('div')
        item.classList.add('panel-list-item')
        item.classList.add('disabled')
        list.appendChild(item)
        let text = document.createElement('div')
        text.classList.add('text')
        text.innerText = data.cache.dane.valid
        item.appendChild(text)
    }
}