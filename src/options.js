document.addEventListener('DOMContentLoaded', () => {
    browser.storage.local.get().then(options => {
        if (options['resolver']) {
            document.querySelector('option[value="' + options['resolver'] + '"]').selected = true
        }
        document.querySelector('input[name="custom"]').disabled = options['resolver'] !== 'custom'
        document.querySelector('input[name="local"]').disabled = options['resolver'] !== 'custom'
        if (options['custom']) {
            document.querySelector('input[name="custom"]').value = options['custom']
        }
        if ('local' in options) {
            document.querySelector('input[name="local"]').checked = options['local']
        }
        if ('notification' in options) {
            document.querySelector('input[name="notification"]').checked = options['notification']
        }
        document.querySelector('input[name="notification"]').disabled = options['dnssec'] && options['dane']
        if ('dnssec' in options) {
            document.querySelector('input[name="dnssec"]').checked = options['dnssec']
        }
        document.querySelector('input[name="paranoia"]').disabled = !options['dnssec']
        if ('paranoia' in options) {
            document.querySelector('input[name="paranoia"]').checked = options['paranoia']
        }
        if ('dane' in options) {
            document.querySelector('input[name="dane"]').checked = options['dane']
        }
    })
})

document.addEventListener('change', (event) => {
    let value = event.target[event.target.type === 'checkbox' ? 'checked' : 'value']
    let options = {}
    if (event.target.name === 'resolver') {
        document.querySelector('input[name="custom"]').disabled = value !== 'custom'
        document.querySelector('input[name="local"]').disabled = value !== 'custom' || !Boolean(document.querySelector('input[name="custom"]').value)
        if (!document.querySelector('input[name="custom"]').value) {
            if (value === 'custom') {
                return document.querySelector('input[name="custom"]').focus()
            } else {
                browser.storage.local.remove('custom')
            }
        }
    } else if (event.target.name === 'custom') {
        document.querySelector('input[name="local"]').disabled = !value
        if (!document.querySelector('input[name="custom"]').value) {
            return document.querySelector('input[name="custom"]').focus()
        }
        options['resolver'] = event.target.name
    } else if (event.target.name === 'dnssec') {
        document.querySelector('input[name="paranoia"]').disabled = !value
        document.querySelector('input[name="notification"]').disabled = value && document.querySelector('input[name="dane"]').checked
    } else if (event.target.name === 'dane') {
        document.querySelector('input[name="notification"]').disabled = value && document.querySelector('input[name="dnssec"]').checked
    }
    options[event.target.name] = value
    browser.storage.local.set(options)
})