const dohjs = require("dohjs");
const asn1js = require("asn1js");
const pkijs = require("pkijs");

const defaultOptions = { resolver: "https://dns.nextdns.io/dns-query", notification: true };
const requestFilter = { urls: ["*://*/*"], types: ["main_frame"] };

let cache = {};
let options = {};
let manuallyAllowedInErrorHostnames = [];
let blockedScreen = browser.runtime.getURL("blocked.html");


/**
 * I. Options management
 */
browser.storage.local.get().then((opts) => {
	options = { ...defaultOptions, ...opts };
	if (opts["resolver"] && opts["resolver"] === "custom") {
		options["resolver"] = opts["custom"];
	}
	if (opts["dnssec"] || opts["dane"]) {
		browser.runtime.onMessage.addListener(blockMessage);
		browser.webRequest.onBeforeRequest.addListener(onBeforeRequest, requestFilter, ["blocking"]);
	}
});

browser.storage.onChanged.addListener((opts) => {
	for (const key of Object.keys(opts)) {
		options[key] = opts[key].newValue;
	}
	if (opts["resolver"]) {
		options["resolver"] = opts["resolver"].newValue === "custom" ? options["custom"] : opts["resolver"].newValue;
		cache = {};
	}
	if (opts["custom"]) {
		options["resolver"] = options["custom"] = opts["custom"].newValue;
		cache = {};
	}
	if (opts["dnssec"] || opts["dane"]) {
		if ((opts["dnssec"] && opts["dnssec"].newValue) || (opts["dane"] && opts["dane"].newValue)) {
			if ((opts["dnssec"] && !options["dane"]) || (opts["dane"] && !options["dnssec"])) {
				browser.runtime.onMessage.addListener(blockMessage);
				browser.webRequest.onBeforeRequest.addListener(onBeforeRequest, requestFilter, ["blocking"]);
			}
		} else {
			if ((opts["dnssec"] && !options["dane"]) || (opts["dane"] && !options["dnssec"])) {
				browser.webRequest.onBeforeRequest.removeListener(onBeforeRequest);
				browser.runtime.onMessage.removeListener(blockMessage);
			}
		}
	}
});


/**
 * II. Icon management
 */
browser.runtime.getPlatformInfo().then((platformInfo) => {
	if (platformInfo.os === "android") {
		// No page action on Android, only whole-page block in case of error
		browser.tabs.onUpdated.addListener(onTabUpdated);
		updatePageAction = () => void 0;
		blockMessage = (message, sender) => {
			manuallyAllowedInErrorHostnames.push(message.hostname);
			browser.tabs.update(sender.tab.id, { url: message.url });
		};
	} else {
		browser.runtime.getBrowserInfo().then((browserInfo) => {
			browser.tabs.onUpdated.addListener(onTabUpdated, { urls: ["*://*/*"], properties: [parseInt(browserInfo.version) > 87 ? "url" : "status"] });
		});
		updatePageAction = (tabId, hostname, secure, error) => {
			if (tabId > -1) {
				const icons = {
					key: {
						d:
							"M512 176.001C512 273.203 433.202 352 336 352c-11.22 0-22.19-1.062-32.827-3.069l-24.012 27.014A23.999 23.999 0 0 1 261.223 384H224v40c0 13.255-10.745 24-24 24h-40v40c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24v-78.059c0-6.365 2.529-12.47 7.029-16.971l161.802-161.802C163.108 213.814 160 195.271 160 176 160 78.798 238.797.001 335.999 0 433.488-.001 512 78.511 512 176.001zM336 128c0 26.51 21.49 48 48 48s48-21.49 48-48-21.49-48-48-48-48 21.49-48 48z",
						h: 512,
						w: 512,
					},
					lock: {
						d:
							"M400 224h-24v-72C376 68.2 307.8 0 224 0S72 68.2 72 152v72H48c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V272c0-26.5-21.5-48-48-48zm-104 0H152v-72c0-39.7 32.3-72 72-72s72 32.3 72 72v72z",
						h: 512,
						w: 448,
					},
					unlock: {
						d:
							"M400 256H152V152.9c0-39.6 31.7-72.5 71.3-72.9 40-.4 72.7 32.1 72.7 72v16c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24v-16C376 68 307.5-.3 223.5 0 139.5.3 72 69.5 72 153.5V256H48c-26.5 0-48 21.5-48 48v160c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z",
						h: 512,
						w: 448,
					},
					open: {
						d:
							"M423.5 0C339.5.3 272 69.5 272 153.5V224H48c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V272c0-26.5-21.5-48-48-48h-48v-71.1c0-39.6 31.7-72.5 71.3-72.9 40-.4 72.7 32.1 72.7 72v80c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24v-80C576 68 507.5-.3 423.5 0z",
						h: 512,
						w: 576,
					},
				};
				let icon;
				switch (true) {
					case !error && cache[hostname].dnssec.valid && Boolean(cache[hostname].dane.valid):
						icon = icons["key"];
						break;
					case !error && cache[hostname].dnssec.valid && !Boolean(cache[hostname].dane.valid):
						icon = icons[cache[hostname].dane.valid === false ? "unlock" : "lock"];
						break;
					default:
						icon = icons["open"];
				}
				let path = new Path2D(icon.d);
				let canvas = document.createElement("canvas").getContext("2d");
				canvas.canvas.width = icon.w;
				canvas.canvas.height = icon.h;
				canvas.fillStyle = error ? "White" : cache[hostname].dnssec.valid ? (cache[hostname].dane.valid === false && secure ? "DarkOrange" : "LimeGreen") : cache[hostname].dnssec.valid == null ? "Gold" : "OrangeRed";
				canvas.fill(path);
				let image = canvas.getImageData(((640 - icon.w) / 2) * -1, ((640 - icon.h) / 2) * -1, 640, 640);
				browser.pageAction.setIcon({ tabId: tabId, imageData: image }).then(() => {
					browser.pageAction.show(tabId).then(() => {
						browser.pageAction.setTitle({
							tabId: tabId,
							title: `DNSSEC/DANE Validator${
								!error && cache[hostname].dnssec.rcode === "NOERROR" && (cache[hostname].dane.valid == null || !secure)
									? ""
									: ": " + (error ? error : cache[hostname].dane.valid == null || !secure ? cache[hostname].dnssec.rcode : cache[hostname].dane.valid ? cache[hostname].dane.valid : "DANE failed")
							}`,
						});
						browser.pageAction.setPopup({ tabId: tabId, popup: "popup.html?data=" + btoa(JSON.stringify({ hostname: hostname, cache: error ? null : cache[hostname], secure: secure, error: error })) });
					});
				});
			}
		};
		blockMessage = (message, sender) => {
			manuallyAllowedInErrorHostnames.push(message.hostname);
			browser.tabs.update(sender.tab.id, { url: message.url, loadReplace: true });
		};
	}
});


/**
 * III. Checks DNSSEC and possibly DANE on page loading
 */

/**
 * As soon as the URL change in a tab, try to get a DNSSEC response for this new hostname
 */
const onTabUpdated = (tabId, changeInfo, tab) => {
	if (changeInfo.url) {
		browser.pageAction.hide(tabId).then(() => {
			getDNSQuality(changeInfo.url)
				.then((hostname) => {
					browser.tabs
						.executeScript(tabId, { code: "window.isSecureContext", runAt: "document_start" })
						.then((secure) => {
							updatePageAction(tabId, hostname, secure[0]);
						})
						.catch((error) => {
							console.error(error);
							updatePageAction(tabId, hostname, false);
						});
					if (options["notification"] && ((cache[hostname].dnssec.valid === false && !options["dnssec"]) || (cache[hostname].dane.valid === false && !options["dane"]))) {
						browser.notifications.create(hostname, { type: "basic", title: `DNSSEC/DANE Validator: ${cache[hostname].dnssec.valid === false ? "DNSSEC invalid" : "DANE failed"}`, message: hostname });
					}
				})
				.catch((error) => {
					updatePageAction(tabId, error.hostname, false, error.reason);
					console.error("Not resolved:", error.hostname, error.reason);
				});
		});
	}
};

/**
 * When headers are received, the TLS connection is already initialised, so get the certificate and try to retrieve TLSA fields
 */
browser.webRequest.onHeadersReceived.addListener(
	(request) => {
		return new Promise((resolve, reject) => {
			browser.webRequest.getSecurityInfo(request.requestId, { certificateChain: true, rawDER: true }).then((securityInfo) => {
				if (securityInfo.state !== "insecure") {
					getDNSQuality(request.url, "TLSA")
						.then((hostname) => {
							checkDANE(securityInfo.certificates, cache[hostname].tlsa, cache[hostname].dnssec.valid == null ? null : cache[hostname].dane.valid).then((daneValid) => {
								cache[hostname].dane.valid = daneValid;
								let result;
								if (!manuallyAllowedInErrorHostnames.includes(hostname) && daneValid === false && options.dane) {
									result = { redirectUrl: blockedScreen + "?data=" + btoa(JSON.stringify({ url: request.url, hostname: hostname, cache: cache[hostname] })) };
								} else if (daneValid != null) {
									updatePageAction(request.tabId, hostname, Boolean(securityInfo.state !== "insecure"));
								}
								resolve(result);
							});
						})
						.catch((error) => {
							updatePageAction(request.tabId, error.hostname, Boolean(securityInfo.state !== "insecure"), error.reason);
							reject("Not resolved: " + error.hostname + " " + error.reason);
						});
				} else {
					resolve();
				}
			});
		});
	},
	requestFilter,
	["blocking"]
);

/**
 * When DNSSEC and/or DANE is configured to block requests, check these securities and possibly redirect to block.html if needed
 */
const onBeforeRequest = (request) => {
	return new Promise((resolve, reject) => {
		getDNSQuality(request.url)
			.then((hostname) => {
				if (!manuallyAllowedInErrorHostnames.includes(hostname) && ((options.dnssec && cache[hostname].dnssec.valid === false) || (options.dnssec && options.paranoia && cache[hostname].dnssec.valid == null) || (options.dane && cache[hostname].dane.valid === false))) {
					resolve({ redirectUrl: blockedScreen + "?data=" + btoa(JSON.stringify({ url: request.url, hostname: hostname, cache: cache[hostname] })) });
				}
				resolve();
			})
			.catch((error) => {
				reject("Not resolved: " + error.hostname + " " + error.reason);
			});
	});
};


/**
 * IV. Internal functions
 */

/**
 * Internal function to check DANE status
 *
 * @uses asn1js
 * @uses pkijs
 * @todo implement TLSA x 0 y z (only x 1 y z is implemented)
 *
 * @param certificates Certificate[]
 * @param tlsaFields Object[]
 * @param result boolean|null result
 * @return boolean|string|null
 */
const checkDANE = async (certificates, tlsaFields, result) => {
	if (tlsaFields) {
		e: for (const tlsaField of tlsaFields) {
			let certificatesToCheck = tlsaField.usage % 2 ? certificates.slice(0, 1) : certificates.slice(1);
			for (const certificate of certificatesToCheck) {
				let asn1certificate = asn1js.fromBER(new Uint8Array(certificate.rawDER).buffer);
				let pkijsCertificate = new pkijs.Certificate({ schema: asn1certificate.result });
				let consideredDataInCertificate = tlsaField.selector === 0 ? pkijsCertificate : pkijsCertificate.subjectPublicKeyInfo;
				if (tlsaField.type === 0) {
					console.debug("raw:", certificate.subject, consideredDataInCertificate.toSchema().toBER(false), consideredDataInCertificate.toSchema().toBER(false) === tlsaField.certificate);
				} else {
					let hash = await crypto.subtle.digest(`SHA-${tlsaField.type === 1 ? 256 : 512}`, consideredDataInCertificate.toSchema().toBER(false));
					let hashArray = Array.from(new Uint8Array(hash));
					let hashString = hashArray.map((digit) => digit.toString(16).padStart(2, "0")).join("");
					result = hashString === tlsaField.certificate;
					if (result) {
						let daneType;
						switch (tlsaField.usage) {
							case 0:
								daneType = "PKIX-TA";
								break;
							case 1:
								daneType = "PKIX-EE";
								break;
							case 2:
								daneType = "DANE-TA";
								break;
							case 3:
								daneType = "DANE-EE";
								break;
						}
						let certificateIssuer = pkijsCertificate.issuer.typesAndValues.map((field) => field.value.valueBlock.value).join(" ");
						result = daneType + " " + certificateIssuer;
						break e;
					}
				}
			}
		}
	}
	return result;
};

/**
 * Internal function to retrieve DNS
 *
 * In case of success, the cache will be populated with the hostname
 *
 * @todo Check it is working correctly for IPv6-only hosts (recordType A does not exist, only AAAA)
 *
 * @param string url URL to obtain DNS (the hostname will be extraced)
 * @param string recordType Only the values "A" or "TLSA" are allowed
 * @return Promise<string,Object> In case of success: the hostname, in case of reject: an object { hostname: string, reason: string }
 */
const getDNSQuality = (url, recordType = "A") => {
	return new Promise((resolve, reject) => {
		let urlObject = new URL(url);
		if ((urlObject.hostname.endsWith(".local") || urlObject.hostname.split(".").length < 2) && (options["resolver"] !== options["custom"] || !options["local"])) {
			reject({ hostname: urlObject.hostname, reason: "Local Domain" });
		} else if (urlObject.hostname.endsWith(".onion") || urlObject.hostname.endsWith(".i2p")) {
			reject({ hostname: urlObject.hostname, reason: "Hidden Service" });
		} else {
			if (
				cache[urlObject.hostname] &&
				((recordType === "A" && cache[urlObject.hostname].dnssec.ttl > Math.floor(Date.now() / 1e3) - cache[urlObject.hostname].dnssec.time) || (recordType === "TLSA" && cache[urlObject.hostname].dane.ttl > Math.floor(Date.now() / 1e3) - cache[urlObject.hostname].dane.time))
			) {
				resolve(urlObject.hostname);
			} else {
				let dnsQuery = dohjs.makeQuery(`${(recordType === "TLSA" ? "_" + (urlObject.port ? urlObject.port : "443") + "._tcp." : "") + urlObject.hostname}`, recordType);
				dnsQuery.additionals = [{ type: "OPT", name: ".", udpPayloadSize: 4096, flags: dohjs.dnsPacket.DNSSEC_OK, options: [] }];
				dohjs.sendDohMsg(dnsQuery, options["resolver"], "POST")
					.then((dnsResponse) => {
						if (!cache[urlObject.hostname]) {
							cache[urlObject.hostname] = { dnssec: {}, dane: {} };
						}
						cache[urlObject.hostname][recordType === "A" ? "dnssec" : "dane"].valid = dnsResponse.rcode === "SERVFAIL" ? false : dnsResponse.flag_ad ? (recordType === "TLSA" && !dnsResponse.answers.length ? null : true) : null;
						cache[urlObject.hostname][recordType === "A" ? "dnssec" : "dane"].time = Math.floor(Date.now() / 1e3);
						cache[urlObject.hostname][recordType === "A" ? "dnssec" : "dane"].ttl = dnsResponse.answers.length ? dnsResponse.answers[0].ttl : -1;
						cache[urlObject.hostname][recordType === "A" ? "dnssec" : "dane"].rcode = dnsResponse.rcode;
						if (recordType === "TLSA" && dnsResponse.flag_ad) {
							cache[urlObject.hostname].tlsa = [];
							dnsResponse.answers.forEach((dnsResponse) => {
								if (dnsResponse.type === "TLSA") {
									cache[urlObject.hostname].tlsa.push({
										usage: dnsResponse.data[0],
										selector: dnsResponse.data[1],
										type: dnsResponse.data[2],
										certificate: Array.from(dnsResponse.data.slice(3))
											.map((str) => str.toString(16).padStart(2, "0"))
											.join(""),
									});
								}
							});
						}
						resolve(urlObject.hostname);
					})
					.catch((error) => {
						reject({ hostname: urlObject.hostname, reason: error.message ? error.message : "Something happened" });
					});
			}
		}
	});
};
