let data = JSON.parse(atob(new URLSearchParams(location.search).getAll('data')))
let html = document.documentElement
let title = document.querySelector('title')
let reason = document.querySelector('#reason')
let link = document.querySelector('a')
let back = document.querySelector('#back')
let load = document.querySelector('#load')

html.style.backgroundColor = data.cache.dane.valid === false ? 'DarkOrange' : data.cache.dnssec.valid == null ? 'Gold' : 'OrangeRed'
html.style.color = data.cache.dnssec.valid == null ? 'Black' : 'White'
title.innerText += reason.innerText = data.cache.dane.valid === false ? 'DANE failed' : data.cache.dnssec.valid == null ? 'No DNSSEC' : 'DNSSEC invalid'
link.href = data.url
link.innerText = data.hostname
link.title = data.hostname
back.disabled = window.history.length === 1
back.addEventListener('click', (e) => {
    e.target.disabled = true
    history.back() // history stacks with every consecutive load???
})
load.addEventListener('click', (e) => {
    e.target.disabled = true
    browser.runtime.sendMessage({
        hostname: data.hostname,
        url: data.url
    })
})