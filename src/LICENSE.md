DoHjs by BYU Internet Measurement and Anti-Abuse Lab (https://github.com/byu-imaal/dohjs/blob/master/LICENSE)  
PKI.js by Peculiar Ventures (https://github.com/PeculiarVentures/PKI.js/blob/master/LICENSE)   
Icons by Font Awesome (https://fontawesome.com/license)